def lesser_of_two_evens(a, b):
    # both numbers are even
    if a % 2 == 0 and b % 2 == 0:
        # return lesser number
        # my solution: return a if a < b else b
        return min(a,b)
    else:
        # return greater number
        # my solution: return a if a > b else b
        return max(a,b)

print(lesser_of_two_evens(2,4))
print(lesser_of_two_evens(2,5))