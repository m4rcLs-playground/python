def animal_crackers(text):
    words = text.lower().split(' ')
    return words[0][0] == words[1][0]

print(animal_crackers('Levelheaded Llama'))
print(animal_crackers('Crazy Kangaroo'))