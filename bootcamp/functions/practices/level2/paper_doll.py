def paper_doll(text):
    thrice = [c+c+c for c in text]
    return ''.join(thrice)

print(paper_doll('Hello'))
print(paper_doll('Mississippi'))