def blackjack(a,b,c):
    result = sum([a,b,c])
    if result > 21:
        if 11 in [a,b,c] and result <= 31:
            result -= 10
        else:
            return 'BUST'
    return result


print(blackjack(5,6,7))
print(blackjack(9,9,9))
print(blackjack(9,9,11))