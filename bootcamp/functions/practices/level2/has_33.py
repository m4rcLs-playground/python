def has_33(nums):
    for i in range(0, len(nums)):
        # my solution: if i-1 >= 0 and nums[i-1] == 3 and nums[i] == 3:
        #     return True
        if nums[i:i+2] == [3,3]:
            return True
    
    return False


print(has_33([1, 3, 3]))
print(has_33([1, 3, 1, 3]))
print(has_33([3, 1, 3]))