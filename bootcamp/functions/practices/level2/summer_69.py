def summer_69(arr):
    result = sum(arr)
    index_diff = 1
    if 6 in arr and 9 in arr:
        index_of_six = 0
        index_of_nine = 0
        for i in range(0, len(arr)):
            if arr[i] == 6:
                index_of_six = i
            elif arr[i] == 9:
                index_of_nine = i
        
        index_diff = index_of_nine - index_of_six
        print("Index diff "+str(index_diff))
        if index_diff >= 1:
            # Because the stop index of tuples is exclusive we have to add +1 to index_of_nine
            result -= sum(arr[index_of_six:index_of_nine+1])

    return result


print(summer_69([1, 3, 5]))
print(summer_69([4, 5, 6, 7, 8, 9]))
print(summer_69([2, 1, 6, 9, 11]))