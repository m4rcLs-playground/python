def master_yoda(text):
    #my solution: return ' '.join(reversed(text.split(' ')))
    characters = text.split()
    return characters[::-1]

print(master_yoda('I am home'))
print(master_yoda('We are ready'))