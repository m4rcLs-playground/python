def almost_there(n):
    return 90 <= n and n <= 110 or 190 <= n and n <= 210

print(almost_there(90))
print(almost_there(104))
print(almost_there(150))
print(almost_there(209))