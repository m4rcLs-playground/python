def up_low(s):
    characters = list(s)
    upper = len(list(filter(lambda c: c.isupper(), characters)))
    lower = len(list(filter(lambda c: c.islower(), characters)))

    print(f"No. of Upper case characters : {upper}")
    print(f"No. of Lower case characters : {lower}")

s = 'Hello Mr. Rogers, how are you this fine Tuesday?'
up_low(s)