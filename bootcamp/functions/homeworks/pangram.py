from string import ascii_lowercase

def ispangram(str1, alphabet=ascii_lowercase):
    alphabet = list(alphabet)
    return len(list(filter(lambda c: c not in str1, alphabet))) == 0

print(ispangram("The quick brown fox jumps over the lay dog"))