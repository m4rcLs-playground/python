def ran_check(num, low, high):
    if num <= high and num >= low:
        print("{} is between {} and {}".format(num, low, high))
    else:
        print("{} is not between {} and {}".format(num, low, high))

def ran_bool(num, low, high):
    return num <= high and num >= low


ran_check(5,2,7)
ran_check(13,1,10)
print(ran_bool(5,2,7))
print(ran_bool(13,1,10))