def multiply(lst):
    product = 1
    for i in lst:
        product *= i
    
    return product if len(lst) > 0 else 0

print(multiply([1,2,3,-4]))
print(multiply([]))