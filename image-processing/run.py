from PIL import ImageGrab
import kivy
from kivy.app import App
from kivy.uix.image import Image
from kivy.clock import Clock
from pprint import pprint

frames_per_second = 10
class ImageViewer(App):
    picture = 'screenshot.jpg'
    image   = None

    def build(self):
        Clock.schedule_interval(self.grab, 1/frames_per_second)
        if self.image is None:
            self.image = Image(source=self.picture)
        return self.image
    

    def grab(self, dt):
        im = ImageGrab.grab()
        im.save(self.picture)
        if self.image is not None:
            self.image.reload()

app = ImageViewer()
app.grab(0)
app.run()
