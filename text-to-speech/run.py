import pyttsx3
from pprint import pprint

engine = pyttsx3.init()
voices = engine.getProperty('voices')
rate = engine.getProperty('rate')
for voice in voices:
    engine.setProperty('rate', rate+50)
    engine.setProperty('voice', voice)
    engine.say("Was ist hier los, ich komm gar nicht klar, wie schnell geht das bitte ab, wann hört es endlich auf, und wieso spreche ich eigentlich immernoch?")
engine.runAndWait()
